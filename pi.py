# Description: This program estimates the value of pi using the Monte Carlo method.

import sys
import time

"""
Args:
    num_points: The number of random points to generate.
Returns:
    The estimated value of pi.
"""
def pi_mc(seed, num_points):
  #hopefully you don't know what this RNG algorithm is
  M1 = 2147483647  
  M = 2147483648 
  A = 48271
  M_sq = M**2
  #Initialize counters
  count = 0
  #Generate random points within the square
  for i in range(num_points):
    seed = (seed*A)%M1
    x_sq = seed**2
    seed = (seed*A)%M1
    y_sq = seed**2
    #Check if the point falls inside the circle
    if (x_sq + y_sq <= M_sq): count += 1
  #Estimate pi using the ratio of points inside the circle to total points
  return 4 * count / num_points

if __name__ == '__main__':
  #how many powers of ten points to generate
  start = time.time()
  power = int(sys.argv[1])
  num_points = 10**power
  seed = 27

  pi_exact = 3.141592653589793238462643
  pi=pi_mc(seed, num_points)
  end = time.time()
  print("MC pi: ", pi)
  print("Error: ", abs(pi_exact-pi))
  print("Time (ms): ", (end-start)*1000)
