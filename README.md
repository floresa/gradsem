# Gradsem

## Getting started
Open BASH

ssh user@monolith.physics.wustl.edu

mkdir demo 

cd demo

git clone https://gitlab.com/floresa/gradsem

cd gradsem 

git branch test

git checkout test 

module load mpi/openmpi-x86_64-py3

top

Helloworldmpi.py

mpirun --oversubscribe -np 16 -mca btl ^openib python3 -m mpi4py /research/user/demo/gradsem/hellompi.py

bashrc

mpirun --oversubscribe -np 16 -mca btl ^openib python3 -host europa-m mpi4py /research/user/demo/gradsem/hellompi.py

mpirun --oversubscribe -np 16 -mca btl ^openib --hostfile machinefile python3 -m mpi4py /research/user/demo/gradsem/hellompi.py

pi.py 

python3 pi.py 7

Pi_mpi.py

mpirun.openmpi --oversubscribe -np 100 -mca btl ^openibpython3 -m mpi4py /research/user/demo/gradsem/pi_mpi.py 7 

pi.F90

change bashrc

module load 

export OMP_NUM_THREADS=2

mpifort -fno-range-check -fopenmp pi.F90 -o pi.x

mpirun.openmpi -np 6 /research/user/demo/gradsem/pi.x

nice